# Nix Development environments

Custom nix development environments, heavily inspired by [dev-templates](https://github.com/the-nix-way/dev-templates).

## Usage

```sh
# start go development environment
nix develop "gitlab:hojerst/nix-envs?dir=go"
```

or add the following to your `.envrc` (see [direnv](https://direnv.net/):

```sh
use flake "gitlab:hojerst/nix-envs?dir=go"
```
