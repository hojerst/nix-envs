{
  description = "Ready-made templates for easily creating flake-driven environments";

  inputs = {
    nixpkgs.url = "https://flakehub.com/f/NixOS/nixpkgs/0.1.*.tar.gz";
    flake-utils.url = "https://flakehub.com/f/numtide/flake-utils/0.1.*.tar.gz";
  };

  outputs = { self, nixpkgs, flake-utils, ... }: {
    templates = rec {
      go = {
        path = ./go;
        description = "Go development environment";
      };
      node = {
        path = ./nof;
        description = "NodeJS development environment";
      };
      python = {
        path = ./python;
        description = "Python development environment";
      };
      web = {
        path = ./web;
        description = "Web development environment";
      };
      infra = {
        path = ./infra;
        description = "Infrastructure development environment";
      };
    };
  };
}
