{
  description = "A Nix-flake-based K8s development environment";

  inputs = {
    nixpkgs.url = "https://flakehub.com/f/NixOS/nixpkgs/0.1.0.tar.gz";
    flake-utils.url = "https://flakehub.com/f/numtide/flake-utils/0.1.*.tar.gz";
  };

  outputs = { self, nixpkgs, flake-utils, ... }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs {
          inherit system;
          config.allowUnfree = true;
        };
        helm = with pkgs ; (wrapHelm kubernetes-helm {
            plugins = with kubernetes-helmPlugins; [
              helm-secrets
              helm-diff
              helm-s3
              helm-git
            ];
          });
      in {
        devShells = rec {
          default = pkgs.mkShellNoCC {
            packages = with pkgs; [
              age
              argocd
              awscli2
              azure-cli
              docker
              fluxcd
              hadolint
              hcloud
              helm
              helmfile
              k3d
              k9s
              krew
              kubecolor
              kubectl
              kustomize
              opentofu
              packer
              restic
              shellcheck
              skopeo
              sops
              terraform
              terraform-docs
              terragrunt
              tflint
              velero
              ytt
            ];
          };
        };
      });
}
